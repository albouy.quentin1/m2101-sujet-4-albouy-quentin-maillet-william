all: main

main: main.c projet.o
	gcc question.o main.c -o main
	
projet.o: projet.h question.c
	gcc -c question.c

clean: 
	rm main
	rm *.o